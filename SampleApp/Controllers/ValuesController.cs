﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SampleApp.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            string val = string.Empty;
            switch (id)
            {
                case 1:
                    val= "I";
                    break;
                case 2:
                    val = "II";
                    break;
                case 3:
                    val = "III";
                    break;
                case 4:
                    val = "IV";
                    break;
                case 5:
                    val = "V";
                    break;
                default:
                    break;
            }
            return val;
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
